﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista5Zad1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
          
            try
            {
                sprDane();
                MessageBox.Show("Zostałeś zarejestrowany.");

            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }


        private void sprDane()
        {
            string email;
            email = email_textBox.Text;
            Regex re = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

            if (!re.IsMatch(email))
                throw new Exception("Podaj prawidłowy adres email.");


            string haslo, haslo2;
            haslo = haslo_textBox.Text;
            haslo2 = haslo2_textBox.Text;

            if (haslo.Length<6)
                throw new Exception("Hasło powinno mieć co najmniej 6 znaków.");
            else if (haslo != haslo2)
                throw new Exception("Podane hasła różnią się.");

            string imie;
            imie = imie_textBox.Text;

            if(imie.Length<1)
                throw new Exception("Podaj imię.");

            string nazwisko;
            nazwisko = nazwisko_textBox.Text;

            if (imie.Length < 1)
                throw new Exception("Podaj nazwisko.");

            int wiek;
            try
            {
                wiek = Convert.ToInt32(wiek_textBox.Text);
            }
            catch
            {
                throw new Exception("Nieprawidłowy format wieku");
            }
            if(wiek < 0 || wiek > 120)
                throw new Exception("Podaj prawidłowy wiek.");


        }


    }
}
