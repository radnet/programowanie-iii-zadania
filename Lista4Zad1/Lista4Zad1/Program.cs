﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista4Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Podaj rozmiar tablicy");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wpisz liczbę");

            int[] tab = new int[n];

            for (int i = 0 ; i < n ; i++)
            {
                Console.Write(i + 1 + ": ");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("\nPosortowana tablica:");
            sort(tab);

            foreach(int i in tab)
                Console.Write(i + ",");

            Console.ReadKey();
        }

        static void sort(int[] tab)
        {
            int n = tab.Length;

            for (int i = 0; i < n; n--)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (tab[j] > tab[j + 1])
                    {
                        int temp = tab[j];
                        tab[j] = tab[j + 1];
                        tab[j + 1] = temp;
                    }
                }
            }
  
        }
    }
}
