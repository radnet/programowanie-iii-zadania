﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista2Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            double suma = 0;

            do
            {
                Console.Write("Podaj z ilu liczb chcesz policzyć średnią: ");
                n = Convert.ToInt32(Console.ReadLine());
            } while (n <= 0);
            
            double[] tab = new double[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write("Podaj liczbe nr "+ (i+1) +": ");
                tab[i] = Convert.ToDouble(Console.ReadLine());
            }

            for(int i = 0; i < n; i++)
                suma += tab[i];
            
            Console.Write("Średnia to "+ suma/n);

            Console.ReadKey();
        }
    }
}
