﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista6Zad1
{

    interface IFigura
    {
        void Rysuj();
        void ObliczPole();
    }

    class Kolo : IFigura
    {
        double promien;

        public Kolo(double promien)
        {
            this.promien = promien;
        }

        public void Rysuj()
        {
            Console.WriteLine("Narysowałem koło.");
        }

        public void ObliczPole()
        {
            Console.WriteLine("Pole: {0:F2}", 2 * Math.PI * promien * promien);
        }
    }

    class Kwadrat : IFigura
    {
        double bok;

        public Kwadrat(double bok)
        {
            this.bok = bok;
        }

        public void Rysuj()
        {
            Console.WriteLine("Narysowałem kwadrat.");
        }

        public void ObliczPole()
        {
            Console.WriteLine("Pole: {0:F2}", bok * bok);
        }


    }


    class Program
    {
        static void Main(string[] args)
        {
            Kolo kolo = new Kolo(4);
            Kwadrat kwadrat = new Kwadrat(8.5);

            kolo.Rysuj();
            kolo.ObliczPole();

            kwadrat.Rysuj();
            kwadrat.ObliczPole();

            Console.ReadKey();
        }
    }
}
