﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista1Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, delta;
 
            do
            {
                Console.Write("Podaj a: ");
                a = Convert.ToDouble(Console.ReadLine());
            } while (a == 0);
            
            Console.Write("Podaj b: ");
            b = Convert.ToDouble(Console.ReadLine());
            Console.Write("Podaj c: ");
            c = Convert.ToDouble(Console.ReadLine());

            delta = b * b - 4 * a * c;

            if(delta < 0)
            {
                Console.WriteLine("Brak rozwiązań w zbiorze liczb rzeczywistych");

            } else if(delta == 0)
            {
                Console.WriteLine("x = {0}", -b * 2 * a);

            } else
            {
                Console.WriteLine("x1 = {0}, x2 = {1}", (-b - Math.Sqrt(delta)) / (2 * a), (-b + Math.Sqrt(delta)) / (2 * a));
            }

            Console.ReadKey();
        }
    }
}
