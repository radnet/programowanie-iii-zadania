﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista5Zad2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int OPEN = 1;
        int CLOSE = 2;
        int SAVE = 3;
        int REFRESH = 4;
        FileStream file;


        public MainWindow()
        {
            InitializeComponent();
            path_textBox.Text = "C:\\Users\\Piotr\\Desktop\\test.txt";
        }

        private void spr_wyjatek(int value)
        {
            try
            {
                
                if (value == OPEN)
                {
                    file = File.Open(path_textBox.Text, FileMode.Open);
                    int length = (int)file.Length;
                    byte[] b = new byte[length];
                    UTF8Encoding temp = new UTF8Encoding(true);
                    file.Read(b, 0, b.Length);
                    content_textBox.Text = temp.GetString(b);
                }
                else if (value == SAVE)
                {
                  
                        file.Close();
                        FileStream file2 = File.Open(path_textBox.Text, FileMode.Create);
                        byte[] info = new UTF8Encoding(true).GetBytes(content_textBox.Text);
                        file2.Write(info, 0, info.Length);
                        file2.Close();
                        spr_wyjatek(OPEN);
                    
                }
                else if (value == REFRESH)
                {
                    file.Close();
                    file = File.Open(path_textBox.Text, FileMode.Open);
                    int length = (int)file.Length;
                    byte[] b = new byte[length];
                    UTF8Encoding temp = new UTF8Encoding(true);
                    file.Read(b, 0, b.Length);
                    content_textBox.Text = temp.GetString(b);
                }
                else if (value == CLOSE)
                {
                    file.Close();
                    file = null;
                    content_textBox.Text = "";
                }

            }

            catch (PathTooLongException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Plik został zamknięty.");
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void open_button_Click(object sender, RoutedEventArgs e)
        {
            spr_wyjatek(OPEN);
        }

        private void close_button_Click(object sender, RoutedEventArgs e)
        {
            spr_wyjatek(CLOSE);
        }

        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            spr_wyjatek(SAVE);
        }

        private void refresh_button_Click(object sender, RoutedEventArgs e)
        {
            spr_wyjatek(REFRESH);
        }
    }
}
