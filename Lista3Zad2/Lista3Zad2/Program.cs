﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista3Zad2
{

    abstract class Osoba
    {
        public String login, haslo, imie, nazwisko;
        protected int rok_ur;

        public abstract void panel();

        public int wiek()
        {
            int rok = DateTime.Now.Year;
            return rok - rok_ur;
        }

        protected void wyswietlDane()
        {
            Console.WriteLine("Imie: " + imie);
            Console.WriteLine("Nazwisko: " + nazwisko);
            Console.WriteLine("Wiek: " + wiek() + " lat");
            Console.WriteLine("login: " + login);
            Console.WriteLine("haslo: " + haslo);
        }

        protected void zmienDane()
        {
            Console.Write("Imie: ");
            imie = Console.ReadLine();
            Console.Write("Nazwisko: " );
            nazwisko = Console.ReadLine();
            Console.Write("Rok urodzenia: ");
            rok_ur = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Dane zostały zmienione");
        }


        protected void zmienHaslo()
        {
            Console.Write("Podaj nowe haslo: ");
            haslo = Console.ReadLine();

            Console.WriteLine("Hasło zostało zmienione");
        }

    }

    class Uzytkownik : Osoba
    {

        public Uzytkownik(String login, String haslo, String imie, String nazwisko, int rok_ur)
        {
            this.login = login;
            this.haslo = haslo;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.rok_ur = rok_ur;
        }

        public override void panel()
        {
            Console.WriteLine("\nWitaj {0} w panelu użytkownika\n", login);
            menu();
           
        }

        protected void menu()
        {
            menuLista();
            obslugaMenu();
        }

        protected virtual void menuLista()
        {
            Console.WriteLine("Menu\n");
            Console.WriteLine("1. Wyświetl swoje dane");
            Console.WriteLine("2. Zmień swoje dane");
            Console.WriteLine("3. Zmień hasło");
            Console.WriteLine("4. Wyloguj się");
        }

        protected virtual void obslugaMenu()
        {
            int a;
            do
            {
                Console.Write("\nPodaj liczbe: ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                if (a == 1)
                    wyswietlDane();
                else if (a == 2)
                    zmienDane();
                else if (a == 3)
                    zmienHaslo();
            } while( a != 4);

        }

    }

    class Administrator : Uzytkownik
    {
        public Administrator(String login, String haslo, String imie, String nazwisko, int rok_u) :
            base(login, haslo, imie, nazwisko, rok_u)
        {
            
        }

        public override void panel()
        {
            Console.WriteLine("\nWitaj {0} w panelu administratora", login);
            menu();
           

        }
        protected override void menuLista()
        {
            base.menuLista();
            Console.WriteLine("5. Wyświetl listę użytkowników");
            Console.WriteLine("6. Wyświetl listę administratorów");
        }

        protected override void obslugaMenu()
        {
            int a;
            do
            {
                Console.Write("\nPodaj liczbe: ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                if (a == 1)
                    wyswietlDane();
                else if (a == 2)
                    zmienDane();
                else if (a == 3)
                    zmienHaslo();
                else if (a == 5)
                    wyswietlUzytkownikow();
                else if (a == 6)
                    wyswietlAdministratorow();
            } while (a != 4);
        }

        private void wyswietlUzytkownikow()
        {
            List<Uzytkownik> uzytkownicy = pobierzUzytkownikow();
            int i = 1;
            foreach (Uzytkownik u in uzytkownicy)
            {
                Console.WriteLine("{0}. {1}, {2} {3}, {4} lat", i, u.login, u.imie, u.nazwisko, u.wiek());
                i++;
            }
        }

        private void wyswietlAdministratorow()
        {
            List<Administrator> administratorzy = pobierzAdministratorow();
            int i = 1;
            foreach (Administrator a in administratorzy)
            {
                Console.WriteLine("{0}. {1}, {2} {3}, {4} lat", i, a.login, a.imie, a.nazwisko, a.wiek());
                i++;
            }
        }

        private List<Uzytkownik> pobierzUzytkownikow()
        {
            return Program.uzytkownicy;
        }

        private List<Administrator> pobierzAdministratorow()
        {
            return Program.administratorzy;
        }



    }

    class Program
    {
        public static List<Uzytkownik> uzytkownicy;
        public static List<Administrator> administratorzy;

        static void Main(string[] args)
        {
            ladujListe();
            logowanie();

            Console.ReadKey();

        }

        static void logowanie()
        {
            int a;

            Console.WriteLine("Zaloguj się do systemu jako");
            Console.WriteLine("1. Użytkownik");
            Console.WriteLine("2. Administrator");
            a = Convert.ToInt32(Console.ReadLine());

            String login, haslo;

            Console.Write("Podaj login: ");
            login = Console.ReadLine();
            Console.Write("Podaj hasło: ");
            haslo = Console.ReadLine();

            if (a == 1)
                logUzytkownik(login, haslo);
            else if (a == 2)
                logAdministrator(login, haslo);
            else
                Console.WriteLine("Nie wybrano poprawnej opcji");
           
        }

        static void logUzytkownik(String login, String haslo)
        {
            Uzytkownik uzytkownik = null;
            foreach (Uzytkownik u in uzytkownicy)
            {
                if (u.login == login && u.haslo == haslo)
                {
                    uzytkownik = u;
                    break;
                }
            }

            if(uzytkownik != null)
                uzytkownik.panel();
            else
                Console.WriteLine("Podano zły login lub hasło");
            
        }

        static void logAdministrator(String login, String haslo)
        {
            Administrator administrator = null;
            foreach (Administrator a in administratorzy)
            {
                if (a.login == login && a.haslo == haslo)
                {
                    administrator = a;
                    break;
                }
            }

            if (administrator != null)
                 administrator.panel();
            else
                Console.WriteLine("Podano zły login lub hasło");
            
        }

        static void ladujListe()
        {
            uzytkownicy = new List<Uzytkownik>();
            uzytkownicy.Add(new Uzytkownik("login", "haslo", "Adam", "Nowak", 1990));
            uzytkownicy.Add(new Uzytkownik("login2", "haslo2", "Jan", "Kwiatkowski", 1993));
            uzytkownicy.Add(new Uzytkownik("login3", "haslo3", "Jacek", "Małecki", 1976));

            administratorzy = new List<Administrator>();
            administratorzy.Add(new Administrator("admin", "admin", "Marek", "Kowalski", 1980));
            administratorzy.Add(new Administrator("admin2", "admin2", "Dawid", "Adamiak", 1996));
        }
    }
}
