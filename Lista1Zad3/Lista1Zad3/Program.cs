﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista1Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, wynik = 1;

            do
            {
                Console.Write("Podaj n: ");
                n = Convert.ToInt32(Console.ReadLine());
            }
            while (n < 0);

            if(n == 0)
                Console.WriteLine(n + "! = 1");
            else
            {
                for (int i = 1; i <= n; i++)
                {
                    wynik *= i;
                }
                Console.WriteLine(n + "! = " + wynik);
            }
            
            Console.ReadKey();
        }
    }
}
