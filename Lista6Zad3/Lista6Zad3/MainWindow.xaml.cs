﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista6Zad3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    
    interface IPrzecena
    {
        string NazwaPrzeceny { get; }
        double ObliczPrzecene(double cena);
    }

    interface IKartaStalegoKlienta
    {
        string NazwaKarty { get; }
        double ObliczZnizke(double cena);
    }

    class Produkt : IPrzecena
    {
        public string nazwa;
        public double cena;
        public double przecena;
        public CheckBox checkBox;

      
        public Produkt(string nazwa, double cena, double przecena, CheckBox checkBox)
        {
            this.nazwa = nazwa;
            this.cena = cena;
            this.przecena = przecena;
            this.checkBox = checkBox;
            checkBox.Content = nazwa;
            checkBox.DataContext = this;
        }

        public string NazwaPrzeceny
        {
            get
            {
                return "Przecena produktu";
            }
        }

        public double ObliczPrzecene(double cena)
        {
            if (przecena != 0)
                return cena * przecena;
            else
                return cena;
        }


    }

    class ZwyklyKlient
    {
        public List<Produkt> listProduktow = new List<Produkt>();
        public double koszt;

        public void dodajProdukt(Produkt p)
        {
            listProduktow.Add(p);
        }

        public void usunProdukt(Produkt p)
        {
            listProduktow.Remove(p);
        }

        public virtual void DoZaplaty()
        {
            foreach(Produkt p in listProduktow)
            {
                koszt += p.ObliczPrzecene(p.cena);
            }
        }
    }

    class StałyKlient : ZwyklyKlient, IKartaStalegoKlienta
    {
        public double znizka;

        public StałyKlient(double znizka)
        {
            this.znizka = znizka;
        }

        public string NazwaKarty
        {
            get
            {
                return "Karta stałego klienta";
            }
        }

        public double ObliczZnizke(double cena)
        {
            if (znizka != 0)
                return cena * (1- znizka);
            else
                return cena;
        }

    }




    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Produkt p1 = new Produkt("Klawiatura", 40, 0.05, cb1);
            Produkt p2 = new Produkt("Notes", 5, 0, cb2);
            Produkt p3 = new Produkt("Płyta CD", 1.50, 0.05, cb3);
            Produkt p4 = new Produkt("Myszka", 30, 0.05, cb4);
            Produkt p5 = new Produkt("Komórka", 250, 0.05, cb5);

            List<Produkt> lista = new List<Produkt>();
           

            ZwyklyKlient zk = new ZwyklyKlient();
            zk.dodajProdukt(p1);
            zk.dodajProdukt(p2);
            zk.dodajProdukt(p5);
            zk.DoZaplaty();

            StałyKlient sk = new StałyKlient(0.2);
            sk.dodajProdukt(p1);
            sk.dodajProdukt(p2);
            sk.dodajProdukt(p5);

            sk.DoZaplaty();
            sk.ObliczZnizke(sk.koszt);

            cena.Content = String.Format("{0:F2}",zk.koszt);
            cena2.Content = String.Format("{0:F2}", sk.ObliczZnizke(sk.koszt));


        }
    }
}
