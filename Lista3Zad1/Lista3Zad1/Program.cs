﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista3Zad1
{

    class Napis
    {
        String tekst;

        public Napis(String tekst)
        {
            this.tekst = tekst;
        }

        public void wyswietlNapis()
        {
            Console.WriteLine(tekst);
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Napis napis1 = new Napis("Napis 1");
            Napis napis2 = new Napis("a to napis 2");
            Napis napis3 = new Napis("wyświetlam napis 3");
            Napis napis4 = new Napis("a tu napis 4");
            Napis napis5 = new Napis("i napis 5");

            napis1.wyswietlNapis();
            napis2.wyswietlNapis();
            napis3.wyswietlNapis();
            napis4.wyswietlNapis();
            napis5.wyswietlNapis();

            Console.ReadKey();
        }
    }
}
