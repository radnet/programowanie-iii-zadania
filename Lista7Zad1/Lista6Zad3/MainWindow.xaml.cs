﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista6Zad3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    
    interface IPrzecena
    {
        string NazwaPrzeceny { get; }
        double ObliczPrzecene(double cena);
    }

    class Produkt : IPrzecena
    {
        public string nazwa;
        public double cena;
        public double przecena;
        public CheckBox checkBox;

      
        public Produkt(string nazwa, double cena, double przecena, CheckBox checkBox)
        {
            this.nazwa = nazwa;
            this.cena = cena;
            this.przecena = przecena;
            this.checkBox = checkBox;
            checkBox.Content = String.Format("{0} {1:F2} zł", nazwa, cena);
            checkBox.DataContext = this;
        }

        public string NazwaPrzeceny
        {
            get
            {
                return "Przecena produktu";
            }
        }

        public double ObliczPrzecene(double cena)
        {
            if (przecena != 0)
                return cena * (1 - przecena);
            else
                return cena;
        }


    }

    class Klient
    {
        public List<Produkt> listProduktow = new List<Produkt>();
        public double koszt;
        public double znizka;

        public Klient(double znizka)
        {
            this.znizka = znizka;
        }

        public void dodajProdukt(Produkt p)
        {
            listProduktow.Add(p);
        }

        public void usunProdukt(Produkt p)
        {
            listProduktow.Remove(p);
        }

        public void DoZaplaty()
        {
            koszt = 0;
            foreach(Produkt p in listProduktow)
            {
                koszt += p.ObliczPrzecene(p.cena);
            }
        }

        public double ObliczZnizke(double cena)
        {
            if (znizka != 0)
                return cena * (1 - znizka);
            else
                return cena;
        }
    }


    public delegate void KlientDelegate(object klient);
    public delegate void ProduktDelegate(object produkt);

    public partial class MainWindow : Window
    {
        public KlientDelegate klientDelegate;
        public ProduktDelegate produktDelegate;

        Klient klient;

        public MainWindow()
        {
            InitializeComponent();

            cb1.Checked += produkt_Checked;
            cb2.Checked += produkt_Checked;
            cb3.Checked += produkt_Checked;
            cb4.Checked += produkt_Checked;
            cb5.Checked += produkt_Checked;

            cb1.Unchecked += produkt_Unchecked;
            cb2.Unchecked += produkt_Unchecked;
            cb3.Unchecked += produkt_Unchecked;
            cb4.Unchecked += produkt_Unchecked;
            cb5.Unchecked += produkt_Unchecked;

            Produkt p1 = new Produkt("Klawiatura", 39.99, 0.05, cb1);
            Produkt p2 = new Produkt("Notes", 5, 0, cb2);
            Produkt p3 = new Produkt("Płyta CD", 1.50, 0.1, cb3);
            Produkt p4 = new Produkt("Myszka", 30, 0.04, cb4);
            Produkt p5 = new Produkt("Komórka", 250, 0.08, cb5);

           klient = new Klient(0);
        }

        public void wybierzKlienta(object klient)
        {
            RadioButton rb = klient as RadioButton;
            if(rb != null)
            {
                if (rb.Name == "radioButton")
                    this.klient.znizka = 0;
                else
                    this.klient.znizka = 0.25;
            }
        }

        public void obliczKoszt(object klient)
        {
            this.klient.DoZaplaty();
            cena.Content = String.Format("{0:F2} zł", this.klient.ObliczZnizke(this.klient.koszt));
        }

        public void dodajProdukt(object produkt)
        {
            CheckBox rb = produkt as CheckBox;
            if (rb != null)
            {
                Produkt pro = rb.DataContext as Produkt;
                if(pro != null)
                {
                    klient.dodajProdukt(pro);
                }
            }
        }

        public void usunProdukt(object produkt)
        {
            CheckBox rb = produkt as CheckBox;
            if (rb != null)
            {
                Produkt pro = rb.DataContext as Produkt;
                if (pro != null)
                {
                    klient.usunProdukt(pro);
                }
            }
        }


        public void Log(object o)
        {
            string output = "";

            if(klient.znizka == 0)
                output += "Zwykły klient \n";
            else
                output += String.Format("Stały klient - Zniżka {0}% \n", klient.znizka * 100);
            foreach(Produkt p in klient.listProduktow)
                output += String.Format("{0} - Promocja {1}% \n", p.nazwa, p.przecena * 100);

            textBox.Text = output;

        }




        public void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            klientDelegate = null;
            klientDelegate += wybierzKlienta;
            klientDelegate += obliczKoszt;
            klientDelegate += Log;
            klientDelegate(sender);

        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            klientDelegate = null;
            klientDelegate += wybierzKlienta;
            klientDelegate += obliczKoszt;
            klientDelegate += Log;
            klientDelegate(sender);
        }

        private void produkt_Checked(object sender, RoutedEventArgs e)
        {
            produktDelegate = null;
            produktDelegate += dodajProdukt;
            produktDelegate += obliczKoszt;
            produktDelegate += Log;
            produktDelegate(sender);
        }

        private void produkt_Unchecked(object sender, RoutedEventArgs e)
        {
            produktDelegate = null;
            produktDelegate += usunProdukt;
            produktDelegate += obliczKoszt;
            produktDelegate += Log;
            produktDelegate(sender);
        }
    }
}
