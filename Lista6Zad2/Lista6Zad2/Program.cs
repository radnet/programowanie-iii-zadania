﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista6Zad2
{

    interface IMotor
    {
        void Start();
        void Stop();
        void Przyspiesz();
        void Zwolnij();
    }

    interface ISamochod
    {
        void Start();
        void Stop();
        void Przyspiesz();
        void Zwolnij();
        void Klimatyzacja();
    }

    class Pojazd : IMotor, ISamochod
    {
        int predkosc, maxPredkosc;
        public Pojazd(int v)
        {
            maxPredkosc = v;
        }

        void IMotor.Start()
        {
            predkosc = 5;
            Console.WriteLine("Motor ruszył. Prędkość: {0}", predkosc); 
        }

        void ISamochod.Start()
        {
            predkosc = 5;
            Console.WriteLine("Samochód ruszył. Prędkość: {0}", predkosc);
        }

        public void Stop()
        {
            Console.WriteLine("Pojazd zatrzymał się");
            predkosc = 0;
        }

        public void Przyspiesz()
        {
            if (predkosc > 0 && predkosc <= maxPredkosc - 15)
            {
                predkosc += 15;
                Console.WriteLine("Pojazd przyśpiesza. Prędkość: {0}", predkosc);
            } else if(predkosc > 0)
            {
                Console.WriteLine("Pojazd nie może już przyśpieszyć. Prędkość: {0}", predkosc);
            } else
            {
                Console.WriteLine("Aby przyśpieszyć musisz ruszyć");
            }
              
        }

        public void Zwolnij()
        {
            if (predkosc >= 20)
            {
                predkosc -= 15;
                Console.WriteLine("Pojazd zwalnia. Prędkość: {0}", predkosc);
            }
            else
            {
                Stop();
            }
        }

        public void Klimatyzacja()
        {
            Console.WriteLine("Klimatyzacja włączona");
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Pojazd pojazd = new Pojazd(120);
            //pojazd.Start();
            IMotor motor = (IMotor)pojazd;
            ISamochod samochod = (ISamochod)pojazd;

            motor.Start();
            motor.Przyspiesz();
            motor.Przyspiesz();
            motor.Przyspiesz();
            motor.Zwolnij();

            Console.WriteLine();

            samochod.Start();
            samochod.Zwolnij();
            samochod.Przyspiesz();
            samochod.Start();
            samochod.Przyspiesz();
            samochod.Przyspiesz();
            samochod.Klimatyzacja();
            samochod.Przyspiesz();
            samochod.Przyspiesz();

            Console.ReadKey();
        }
    }
}
