﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista4Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[,] tabWybrane = new int[6, 2];
            Console.WriteLine("Podaj 6 liczb od 1 do 49");
            int i = 0;
            do
            {
                int a = Convert.ToInt32(Console.ReadLine());
                bool wpisz = true;
                for (int j = 0; j <= i; j++)
                    if (tabWybrane[j,0] == a)
                        wpisz = false;
                if (a <= 49 && a > 0 && wpisz)
                {
                    tabWybrane[i, 0] = a;
                    i++;
                }

            } while (i < 6);


            int[] tabLosowe = { 0, 0, 0, 0, 0, 0 };
            i = 0;
            do
            {
                int a = rnd.Next(1, 50);
                bool wpisz = true;
                for (int j = 0; j < 6; j++)
                    if (tabLosowe[j] == a)
                        wpisz = false;
                if(wpisz)
                {
                    tabLosowe[i] = a;
                    i++;
                }
            } while (i < 6);


            Console.WriteLine("\nWylosowano 6 liczb:\n");

            foreach(int a in tabLosowe)
            {
                Console.Write("{0}," , a);
            }

            int ile = 0;

            for (i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    if (tabWybrane[j, 0] == tabLosowe[i])
                    {
                        tabWybrane[j, 1] = 1;
                        ile++;
                    }

            Console.Write("\n\nIlość trafień: {0} - ", ile);
            for (i = 0; i < 6; i++)
                if (tabWybrane[i, 1] == 1)
                    Console.Write("{0},", tabWybrane[i, 0]);

            Console.ReadKey();

        }
    }
}
