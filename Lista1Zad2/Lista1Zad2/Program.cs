﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista1Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            do
            {
                Console.Write("Podaj, który wyraz ciągu Fibonacciego obliczyć: ");
                n = Convert.ToInt32(Console.ReadLine());
            }
            while (n < 0);

            Console.WriteLine( "Wyraz ten to: " + Fibo(n) );

            Console.ReadKey();
        }

        static int Fibo(int n)
        {
            if (n == 0)
                return 0;
            else if (n == 1 || n == 2)
                return 1;
            else
                return Fibo(n - 2) + Fibo(n - 1);
        }

    }
}
