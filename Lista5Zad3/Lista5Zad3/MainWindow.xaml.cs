﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista5Zad3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                checkOrder();
                MessageBox.Show("Zapisano");
            }
            catch(OrderException ex)
            {
                MessageBox.Show(ex.Source + ": " + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void checkOrder()
        {
            int n = 5;
            int[] numbers = new int[n];
            try
            {
                numbers[0] = Convert.ToInt32(textBox_1.Text);
                numbers[1] = Convert.ToInt32(textBox_2.Text);
                numbers[2] = Convert.ToInt32(textBox_3.Text);
                numbers[3] = Convert.ToInt32(textBox_4.Text);
                numbers[4] = Convert.ToInt32(textBox_5.Text);
            } 
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    if (i != j && numbers[i] == numbers[j])
                        throw new OrderException("Liczby się powtarzają.");
                

            int suma1 = n * (n + 1) / 2;
            int suma2 = 0;
            for (int i = 0; i < 5; i++)
                suma2 += numbers[i];

            if (suma1 != suma2)
                throw new OrderException("Nieprawidłowa kolejność liczb.");

        }
    }
}
