﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista4Zad2
{
    class Program
    {
        static int[,] tab1;
        static int[,] tab2;
        static int[,] tab3;

        static void Main(string[] args)
        {
            int rows, cols;
            Console.WriteLine("\nPodaj wymiary 1 macierzy:");
            Console.Write("Wiersze:");
            rows = Convert.ToInt32(Console.ReadLine());
            Console.Write("Kolumny:");
            cols = Convert.ToInt32(Console.ReadLine());
            tab1 = new int[rows, cols];

            Console.WriteLine("\nWpisz liczby:");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)

                {
                    Console.Write("[{0},{1}]: ", i, j);

                    tab1[i, j] = Convert.ToInt32(Console.ReadLine());

                }
            }

            Console.WriteLine("\nPodaj wymiary 2 macierzy:");
            Console.Write("Wiersze:");
            rows = Convert.ToInt32(Console.ReadLine());
            Console.Write("Kolumny:");
            cols = Convert.ToInt32(Console.ReadLine());
            tab2 = new int[rows, cols];

            Console.WriteLine("\nWpisz liczby:");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)

                {
                    Console.Write("[{0},{1}]: ", i, j);

                    tab2[i, j] = Convert.ToInt32(Console.ReadLine());

                }
            }

            Console.WriteLine("1. Dodawanie");
            Console.WriteLine("2. Mnożenie");
            int c = Convert.ToInt32(Console.ReadLine());

            if (c == 1)
                dodawanie();
            else
                mnozenie();

            Console.ReadKey();

        }

        static void dodawanie()
        {
            if (tab1.GetLength(0) == tab2.GetLength(0) && tab1.GetLength(1) == tab2.GetLength(1))
            {
                tab3 = new int[tab1.GetLength(0), tab1.GetLength(1)];
                for (int i = 0; i < tab1.GetLength(0); i++)
                    for (int j = 0; j < tab1.GetLength(1); j++)
                        tab3[i, j] = tab1[i, j] + tab2[i, j];

                wyswietl();
            }
            else
                Console.WriteLine("Błąd");
            
        }

        static void mnozenie()
        {
            if (tab1.GetLength(1) == tab2.GetLength(0))
            {
                tab3 = new int[tab1.GetLength(0), tab2.GetLength(1)];
                for (int i = 0; i < tab3.GetLength(0); i++)
                    for (int j = 0; j < tab3.GetLength(1); j++)
                    {
                        int temp = 0;
                        for(int k = 0; k < tab1.GetLength(1); k++)
                            temp += tab1[i, k] + tab2[k, j];
                        tab3[i, j] = temp;
                    }
                wyswietl();
            }
            else
                Console.WriteLine("Błąd");

        }
        static void wyswietl()
        {
            for (int i = 0; i < tab3.GetLength(0); i++)
            {
                for (int j = 0; j < tab3.GetLength(1); j++)
                    Console.Write("\t{0}", tab3[i, j]);

                Console.WriteLine("\n");
            }
        }



    }
}
