﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista7Zad3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public delegate void SendTextDelegate(String text);
    public delegate void RefreshTextDelegate(String text);

    public partial class MainWindow : Window
    {
        public event RefreshTextDelegate RefreshEvent;

        int i;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddNewWindow()
        {
            i++;
            ShoutBox sb = new ShoutBox(this);
            sb.TextEvent += new SendTextDelegate(Refresh);
            sb.nick.Text = "nick_" + i;
            sb.Show();
           
        }

        public void OnRefreshEvent(String text)
        {
            if (RefreshEvent != null)
                RefreshEvent(text);
        }

        private void Refresh(string text)
        {
            OnRefreshEvent(text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddNewWindow();
        }
    }
}
