﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lista7Zad3
{
    /// <summary>
    /// Interaction logic for ShoutBox.xaml
    /// </summary>
    public partial class ShoutBox : Window
    {
        public event SendTextDelegate TextEvent;

        MainWindow main;

        public ShoutBox(MainWindow main)
        {
            InitializeComponent();
            this.main = main;
            main.RefreshEvent += new RefreshTextDelegate(Refresh);
        }

        private void Refresh(string text)
        {
            Content.Text += text;
        }

        public void OnTextEvent(String text)
        {
            if (TextEvent != null)
            {
                TextEvent(String.Format("{0}:  {1}\n", nick.Text, newText.Text));
                newText.Text = "";
                scroll.ScrollToBottom();
            }   
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OnTextEvent(newText.Text);
        }

        private void newText_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                OnTextEvent(newText.Text);
        }


    }
}
