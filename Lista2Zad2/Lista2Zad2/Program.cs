﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lista2Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            int figura;

            do
            {
                Console.WriteLine("Wybierz figurę:\n");
                Console.WriteLine(" 1. Kwadrat");
                Console.WriteLine(" 2. Prostokąt");
                Console.WriteLine(" 3. Trojkąt");
                Console.WriteLine(" 4. Koło\n");
                figura = Convert.ToInt32(Console.ReadLine());
            } while (figura < 1 || figura > 4);

            if (figura == 1)
                kwadrat();
            else if (figura == 2)
                prostokat();
            else if (figura == 3)
                trojkat();
            else
                kolo();

            Console.ReadKey();
        }

        static void kwadrat()
        {
            double a;
            do
            {
                Console.Write("Podaj długość boku: ");
                a = Convert.ToDouble(Console.ReadLine());
            } while (a <= 0);
            
            Console.WriteLine("Obwód = "+ 4*a);
            Console.WriteLine("Pole = " + a*a);
        }

        static void prostokat()
        {
            double a, b;
            do
            {
                Console.Write("Podaj długość boku a: ");
                a = Convert.ToDouble(Console.ReadLine());
            } while (a <= 0);
            do
            {
                Console.Write("Podaj długość boku b: ");
                b = Convert.ToDouble(Console.ReadLine());
            } while (b <= 0);

            Console.WriteLine("Obwód = " + (2*a + 2*b));
            Console.WriteLine("Pole = " + a * b);
        }

        static void trojkat()
        {
            double a, b, c, p;
            do
            {
                Console.Write("Podaj długość boku a: ");
                a = Convert.ToDouble(Console.ReadLine());
                Console.Write("Podaj długość boku b: ");
                b = Convert.ToDouble(Console.ReadLine());
                Console.Write("Podaj długość boku c: ");
                c = Convert.ToDouble(Console.ReadLine());
            } while ( !(a+b>c && a+c>b && b+c>a) );

            p = (a + b + c) / 2;

            Console.WriteLine("Obwód = " + 2*p);
            Console.WriteLine("Pole = " + ( Math.Sqrt(p*(p-a)*(p-b)*(p-c)) ));
        }

        static void kolo()
        {
            double r;
            do
            {
                Console.Write("Podaj promień: ");
                r = Convert.ToDouble(Console.ReadLine());
            } while (r <= 0);

            Console.WriteLine("Obwód = " + 2* Math.PI *r);
            Console.WriteLine("Pole = " + Math.PI *r*r);
        }

    }

}
