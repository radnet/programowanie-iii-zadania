﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lista7Zad2
{

    public class Lotto
    {
        public delegate void LottoDelegate(object lotto, LottoEventArgs arg);
        public event LottoDelegate LottoEvent;
        private int[] tabLosowe = { 0, 0, 0, 0, 0, 0 };

        private void OnLottoEvent(object lotto, LottoEventArgs arg)
        {
            if (LottoEvent != null)
                LottoEvent(this, arg);
        }

        public async void Losuj()
        {
            Random rnd = new Random();
            int i = 0;
            do
            {
                int a = rnd.Next(1, 50);
                bool wpisz = true;
                for (int j = 0; j < 6; j++)
                    if (tabLosowe[j] == a)
                        wpisz = false;
                if (wpisz)
                {
                    await Task.Delay(1000);

                    tabLosowe[i] = a;
                    i++;
                    LottoEventArgs arg = new LottoEventArgs(a);
                    OnLottoEvent(this, arg);
                }
            } while (i < 6);

        }
    }

    public class LottoEventArgs : EventArgs
    {
        public int numer;
        public LottoEventArgs(int nr)
        {
            numer = nr;
        }  
    }

    public partial class MainWindow : Window
    {
        Lotto lotto;
        int i;
        public MainWindow()
        {
            InitializeComponent();

            lotto = new Lotto();
            losuj.Click += Losuj_Click;
            lotto.LottoEvent += new Lotto.LottoDelegate(Wylosowano);
        }

        private void Losuj_Click(object sender, RoutedEventArgs e)
        {
            losuj.Visibility = Visibility.Hidden;
            losowanie.Visibility = Visibility.Visible;
            lotto.Losuj();
        }

        public void Wylosowano(object lotto, LottoEventArgs arg)
        {
            liczby.Text = String.Format("{0}   {1}", liczby.Text, arg.numer);
            i++;
            if(i == 6)
                losowanie.Text = "Koniec losowania";
        }
    }
}
